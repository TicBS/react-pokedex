import React from 'react';

const Header = () => (
  <header className="">
    <div className="container">
      <div className="row align-items-center">
        <div className="col-12">
          <h3>React-Pokédex</h3>
        </div>
      </div>
    </div>
  </header>
);

export default Header;
