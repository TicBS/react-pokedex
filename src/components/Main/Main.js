import React from 'react';
import Pokedex from './Pokedex';

class Main extends React.Component {
  render() {
    return(
      <div className="main">
        <div className="container">
          <div className="row">
            <Pokedex />
          </div>
        </div>
      </div>
    )
  }
}

export default Main;
